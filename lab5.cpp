/*
 * File: lab5.cpp
 * Name: Tyler Hochrine
 * Username: thochri
 * Lab #: Lab 5
 * Section #: 006
 * TA: Anurata Hridi
 */


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;

//sets values to different suits
//helps with sorting
enum Suit 
{ 
	SPADES = 0, 
	HEARTS = 1, 
	DIAMONDS = 2, 
	CLUBS = 3 
};

//card struct to make each card
typedef struct Card 
{
	Suit suit;
	int value;
} Card;

//prototype functions for functions below
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
//rand function to shuffle the cards
int myrandom (int i) 
{ 
	return std::rand()%i;
}

//main function sets the deck, shuffles the deck, sorts the hand, and displays
//the hand
int main(int argc, char const *argv[]) 
{
	//a deck of cards has 52 cards and the hand has 5 cards
	Card deck[52];
	Card hand[5];
	int count = 0;

	//setting a seed for the random_shuffle function
	srand(unsigned (time(0)));

	//setting each value in the deck array to a different card
	//i goes from 0 - 3 for the 4 different suits
	for(int i = 0; i < 4; i++)
	{
		//j goes from 0 - 12 for the 13 different value types
		for(int j = 0; j < 13; j++)
		{
			deck[count].suit = static_cast<Suit>(i); 
			deck[count].value = j + 2;
			count++;
		}
	}

	//shuffles the deck
	random_shuffle(&deck[0], &deck[52], myrandom);

	//setting the hand to the first 5 cards of the shuffled deck
	for(int i = 0; i < 5; i++)
	{
		hand[i] = deck[i];
	}

	//using the built-in sort function to sort the hand based on the suit_order
	//function defined below
	sort(&hand[0], &hand[5], suit_order);

	//printing out the now sorted hand
	for(int i = 0; i < 5; i++)
	{
		cout << right << setw(10) << get_card_name(hand[i]) << " of " 
			<< get_suit_code(hand[i]) << endl;
	}

	return 0;
}

//this function sorts the hand based on the suit first and then the value
//takes in 2 cards and compares their suit. If they are equal, then it compares
//their value
bool suit_order(const Card& lhs, const Card& rhs) 
{
	//sorting the suit
	if(lhs.suit < rhs.suit)
	{
		return true;
	}
	else if(lhs.suit > rhs.suit)
	{
		return false;
	}
	else
	{
		//sorting the value
		if(lhs.value < rhs.value)
		{
			return true;
		}
		else if(lhs.value > rhs.value)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}

//takes in the card suit and returns the unicode character based on the value
//defined in the enum above to display the correct suit
string get_suit_code(Card& c) 
{
	switch (c.suit) 
	{
		case SPADES:    return "\u2660";
		case HEARTS:    return "\u2661";
		case DIAMONDS:  return "\u2662";
		case CLUBS:     return "\u2663";
		default:        return "";
	}
}

//takes in the card value as an int and returns the string version of the card 
//value. Changes 11 - 14 to Jack - Ace
string get_card_name(Card& c) 
{
	switch(c.value)
	{
		case 2:    return "2";
		case 3:    return "3";
		case 4:    return "4";
		case 5:    return "5";
		case 6:    return "6";
		case 7:    return "7";
		case 8:    return "8";
		case 9:    return "9";
		case 10:   return "10";
		case 11:   return "Jack";
		case 12:   return "Queen";
		case 13:   return "King";
		case 14:   return "Ace";
		default:   return "";
	}
}
